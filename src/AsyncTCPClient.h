#ifndef ASYNCTCPCLIENT_ASYNCTCPCLIENT_H
#define ASYNCTCPCLIENT_ASYNCTCPCLIENT_H

#include <iostream>
#include <memory>
#include <mutex>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

typedef void(*Callback)(unsigned int request_id, std::string& response, boost::system::error_code ec);

struct Session {
    boost::asio::ip::tcp::socket _socket;
    boost::system::error_code _ec;
    boost::asio::ip::tcp::endpoint _ep;
    std::string _request;
    unsigned int _request_id;
    boost::asio::streambuf _response_buf;
    std::string _response;
    bool _isCancelled;
    std::mutex _cancel_guard;
    Callback _callback;


    Session(boost::asio::io_context& ioc, const std::string& raw_ip_address, unsigned short port_num,
            std::string& request, unsigned int request_id, Callback callback)
            : _socket(ioc), _ep(boost::asio::ip::address::from_string(raw_ip_address), port_num),
              _request(request), _request_id(request_id), _isCancelled(false), _callback(callback)
    {
    }
};

class AsyncTCPClient {
private:
    boost::asio::io_context _ioc;
    std::unique_ptr<boost::asio::io_context::work> _work;
    boost::thread_group _thread_pool;
    std::map<int, std::shared_ptr<Session>> _sessions;
    std::mutex _session_guard;

    void onRequestComplete(std::shared_ptr<Session> session);

public:
    AsyncTCPClient(unsigned int threads_num=3);

    void sendRequest(std::string request, const std::string &raw_ip_address, unsigned short port_num, Callback callback, unsigned int request_id);
    void onConnect(const boost::system::error_code& ec, std::shared_ptr<Session> session);
    void onRequestSent(const boost::system::error_code& ec, std::size_t bytes_transferred, std::shared_ptr<Session> session);
    void onResponseReceived(const boost::system::error_code& ec, std::size_t bytes_transferred, std::shared_ptr<Session> session);
    void cancelRequest(unsigned int request_id);
    void close();
};

#endif //ASYNCTCPCLIENT_ASYNCTCPCLIENT_H
