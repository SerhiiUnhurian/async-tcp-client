#include "AsyncTCPClient.h"
#include <boost/asio.hpp>
#include <memory>
#include <mutex>

AsyncTCPClient::AsyncTCPClient(unsigned int threads_num) {
    assert(threads_num > 0);

    _work = std::make_unique<boost::asio::io_context::work>(_ioc);

    for(unsigned int i = 0; i < threads_num; i++) {
        _thread_pool.create_thread([this](){
            _ioc.run();
        });
    }
}

void AsyncTCPClient::onRequestComplete(std::shared_ptr<Session> session) {
    boost::system::error_code ignored_ec;

    session->_socket.shutdown(boost::asio::socket_base::shutdown_both, ignored_ec);

    std::unique_lock<std::mutex> session_lock(_session_guard);

    auto it = _sessions.find(session->_request_id);
    if (it != _sessions.end()) {
        _sessions.erase(it);
    }

    session_lock.unlock();

    if (session->_ec.value() == 0 && session->_isCancelled) {
        session->_ec = boost::asio::error::operation_aborted;
    }

    session->_callback(session->_request_id, session->_response, session->_ec);
}

void AsyncTCPClient::sendRequest(std::string request, const std::string &raw_ip_address, unsigned short port_num, Callback callback,
                 unsigned int request_id) {
    request += '\n';
    std::shared_ptr<Session> session { std::make_shared<Session>(_ioc, raw_ip_address, port_num, request, request_id, callback) };

    std::unique_lock<std::mutex> session_lock(_session_guard);
    _sessions[request_id] = session;
    session_lock.unlock();

    session->_socket.async_connect(session->_ep, [this, session](const boost::system::error_code& ec){
        onConnect(ec, session);
    });
}

void AsyncTCPClient::onConnect(const boost::system::error_code& ec, std::shared_ptr<Session> session) {
    if (ec.value() != 0) {
        session->_ec = ec;
        onRequestComplete(session);
        return;
    }

    std::unique_lock<std::mutex> cancle_lock(session->_cancel_guard);

    if(session->_isCancelled) {
        onRequestComplete(session);
        return;
    }

    boost::asio::async_write(session->_socket, boost::asio::buffer(session->_request),
                             [this, session](const boost::system::error_code& ec, std::size_t bytes_transferred){
                                 onRequestSent(ec, bytes_transferred, session);
                             });
}

void AsyncTCPClient::onRequestSent(const boost::system::error_code& ec, std::size_t bytes_transferred, std::shared_ptr<Session> session) {
    if (ec.value() != 0) {
        session->_ec = ec;
        onRequestComplete(session);
        return;
    }

    std::unique_lock<std::mutex> cancle_lock(session->_cancel_guard);

    if(session->_isCancelled) {
        onRequestComplete(session);
        return;
    }

    boost::asio::async_read_until(session->_socket, session->_response_buf, '\n',
                                  [this, session](const boost::system::error_code& ec, std::size_t bytes_transferred){
                                      onResponseReceived(ec, bytes_transferred, session);
                                  });
}

void AsyncTCPClient::onResponseReceived(const boost::system::error_code& ec, std::size_t bytes_transferred, std::shared_ptr<Session> session) {
    if (ec.value() != 0) {
        session->_ec = ec;
    } else {
        std::istream strm(&session->_response_buf);
        std::getline(strm, session->_response);
    }
    onRequestComplete(session);
}

void AsyncTCPClient::cancelRequest(unsigned int request_id) {
    std::unique_lock<std::mutex> session_lock(_session_guard);

    auto it = _sessions.find(request_id);
    if (it != _sessions.end()) {
        auto session = it->second;
        std::unique_lock<std::mutex> cancel_lock(session->_cancel_guard);

        session->_isCancelled = true;
        session->_socket.cancel();
    }
}

void AsyncTCPClient::close() {
    _work.reset();
    _thread_pool.join_all();
}