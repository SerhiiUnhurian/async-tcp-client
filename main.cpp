#include <iostream>
#include <thread>

#include "src/AsyncTCPClient.h"

void handler(unsigned int request_id, std::string& response, boost::system::error_code ec) {
    if (ec.value() == 0) {
        std::cout << "Request #" << request_id
                  << " has completed. Response: "
                  << response << std::endl;
    } else if (ec.value() == boost::asio::error::operation_aborted) {
        std::cout << "Request #" << request_id
                  << " has been cancelled by the user."
                  << std::endl;
    } else{
        std::cout << "Request #" << request_id
                  << " failed! Error code = " << ec.value()
                  << ". Error message = " << ec.message()
                  << std::endl;
    }
}

int main() {
    unsigned short port_num = 3333;
    std::string raw_ip_address = "127.0.0.1";
    AsyncTCPClient client;

    try {
        client.sendRequest("add name Serge", raw_ip_address, port_num,
                           handler, 1);

        std::this_thread::sleep_for(std::chrono::seconds(2));

        client.sendRequest("add surname Unguryan", raw_ip_address, port_num,
                           handler, 2);

        std::this_thread::sleep_for(std::chrono::seconds(2));

        client.sendRequest("delete surname", raw_ip_address, port_num,
                           handler, 3);

        std::this_thread::sleep_for(std::chrono::seconds(2));

        client.sendRequest("set name John", raw_ip_address, port_num,
                           handler, 3);

        std::this_thread::sleep_for(std::chrono::seconds(2));
        client.close();
    }
    catch (boost::system::system_error &e) {
        std::cout << "Error occurred! Error code = " << e.code()
                  << ". Message: " << e.what();

        return e.code().value();
    }

    return 0;
}
