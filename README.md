## Client-server application

#### This repository contains source files for Part 2 of the main task.

Main task:

Part 1. Server application.  

Server should work with one of the following file format: XML/JSON/YAML, that contains pairs of tag_name and tag_value. The Server should read the pairs, add new pairs and update the existing values.
Client is a console application. 

Part 2. Client application

Clients connect to the Server using sockets, send requests and receive answers from the Server. Users should be able to use console commands to get access to XML file. You need to implement at least three commands:  

add <tag_name> <tag_value>  
get <tag_name>  
set <tag_name> <tag_value  
delete <tag_name>  

Advanced task:

* Each console command should echo in the Server console window and show a reply from the Server in the Client’s console window.
* Develop second flavour of client application: Monitor. It should connect to the server and echo each command that it processed. It should not interact with users and send commands to the Server.
* Cover your code by unit tests (like Google Test).
* Design the Server as a Linux daemon.

Additional requirements:

* Use C++ and boost asio in async mode. Avoid C-style code.
* Use STL (std::vector, std::map, etc) as much as possible.
* It is very good if you implement XML parser (it does not need to cover the whole XML standard, you need just parse tag – value pairs) but you can use any well-known libraries (example, TinyXML).